package hw1.b;

import java.util.Arrays;
import java.util.Random;

public class Solution3 {
	 public static double[][] matrixMultiply2(double[][] m1, double[][] m2) {
	        int r = m1.length;
	        int c = m2[0].length;
	        m2 = matrixTranspose(m2);
	        double[][] result = new double[r][c];
	        for (int i = 0; i < r; i++) {
	            for (int j = 0; j < c; j++) {
	                double temp = 0;
	                for (int k = 0; k < m1[0].length; k++) {
	                    double i1 = m1[i][k];
	                    double i2 = m2[j][k];
	                    temp += i1 * i2;
	                }
	                result[i][j] = temp;
	            }
	        }
	        return result;
	    }
	 
	 public static double[][] matrixTranspose(double[][] m) {
	        int r = m.length;
	        int c = m[0].length;
	        double[][] result = new double[c][r];
	        for (int i = 0; i < r; i++) {
	            for (int j = 0; j < c; j++) {
	                result[j][i] = m[i][j];
	            }
	        }
	        return result;
	    }


	    public static double[][] randomDoubleMatrix(int r, int c) {
	        Random random = new Random();
	        double[][] result = new double[r][c];
	        for (int i = 0; i < r; i++) {
	            for (int j = 0; j < c; j++) {
	                result[i][j] = random.nextDouble() * 1000;
	            }
	        }
	        return result;
	    }
	    
	    public static void main(String[] args) {
	        long start = System.currentTimeMillis();
	        int times = 10;
	        for (int i = 0; i < times; i++) {
	        	   double[][] intM1 = randomDoubleMatrix(1000, 600);
	               double[][] intM2 = randomDoubleMatrix(600, 700);
	               matrixMultiply2(intM1, intM2);
	           }
	           long end = System.currentTimeMillis();
	           long cost = (end - start);
	           System.out.println("Double matrix multiply algorithm2: size (1000x600)x(600x700)");
	           System.out.println("average time: " + cost / times + "ms");
	           System.out.println();
	        }
}

