.data

var1: .word 2
var2: .word 6
var3: .word -2020

.text
.globl main
main:
	lw $t0, var1               #$t0 <- var1
	lw $t1, var2               #$t1 <- var2
	bne $t0, $t1, else         # if $t0 != $t1, goto 'else' lable
	
	lw $t2, var3
	sw $t2, var1
	sw $t2, var2
	beq $0, $0, EXIT      # goto 'EXIT' lable unconditionaly
	
else:
	move $t3, $t0         # swap $t0 and $t1
	move $t0, $t1
	move $t1, $t3
	sw $t0, var1          # save new values to their address
	sw $t1, var2 

EXIT:
	li $v0, 1		      # system call print int
	lw $a0, var1
	syscall				  # execute
	li $v0, 1
	lw $a0, var2
	syscall
	jr $ra