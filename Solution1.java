package hw1.a;

import java.util.Arrays;
import java.util.Random;

public class Solution1 {
	 public static double[][] matrixMultiply(double[][] m1, double[][] m2) {
	        int r = m1.length;
	        int c = m2[0].length;
	        double[][] result = new double[r][c];
	        for (int i = 0; i < r; i++) {
	            for (int j = 0; j < c; j++) {
	                double temp = 0;
	                for (int k = 0; k < m1[0].length; k++) {
	                    double i1 = m1[i][k];

	                    double i2 = m2[k][j];
	                    temp += i1 * i2;
	                }
	                result[i][j] = temp;
	            }
	        }
	        return result;
	    }

	    public static double[][] randomDoubleMatrix(int r, int c) {
	        Random random = new Random();
	        double[][] result = new double[r][c];
	        for (int i = 0; i < r; i++) {
	            for (int j = 0; j < c; j++) {
	                result[i][j] = random.nextDouble() * 1000;
	            }
	        }
	        return result;
	    }
	    
public static void main(String[] args) {
    long start = System.currentTimeMillis();
    int times = 10;
    for (int i = 0; i < times; i++) {
    	   double[][] intM1 = randomDoubleMatrix(1000, 600);
           double[][] intM2 = randomDoubleMatrix(600, 700);
           matrixMultiply(intM1, intM2);
       }
       long end = System.currentTimeMillis();
       long cost = (end - start);
       System.out.println("Double matrix multiply: size (1000x600)x(600x700)");
       System.out.println("average time: " + cost / times + "ms");
       System.out.println();
    }
}

